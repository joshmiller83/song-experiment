# Acts 13-14

God came to earth  
As a man  
He worked hard, got slapped  
He healed hurts and preached forgiveness  
Showed us sin starts in the mind

Share the news  
Let others know:  
God lives, laid his perfect life down  
For you, for me!

He gave us life everlasting,   
A fountain where our cup overflows,  
Life giving faith in our creator who cares  
Rejoice! Boldly testify, a focus anew!

A hope is found, this time sound  
Stand stand, leap around  
Jesus is the super, the ultra perfect grouper  
Dance, awe inspired, we’ve been found

Share share, give us another round  
Our advocate cleaned us up, paid our debt    
My story, your story, open the doors  
God came here to adopt us, make us whole



---

Lyrics By Josh Miller, July 2019   
_Initial lyrics inspired by [sermon on Acts 13-14](https://subsplash.com/the-church-at-cherrydale/sermons/mi/+vfn594k) by Brandon Simpson._

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.