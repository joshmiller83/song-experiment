# Song Experiment

A simple repository of groups of words that I write. Generally inspired by sermons and written either in tandem with hearing a sermon or reading the word. Maybe one day a song will come out of this.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />All work in this repository is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. This means I gladly and humbly allow anyone (commercial or otherwise) to use or rework my work as long as they share it with others.
